#!usr/local/env bash
cp 0_NeuralNetwork_CreateSchema.sql.pattern 0_NeuralNetwork_CreateSchema.sql
cp 1_NeuralNetwork_DDL.sql.pattern 1_NeuralNetwork_DDL.sql
sed -i "s/{NEURAL_NETWORK_SCHEMA}/${NEURAL_NETWORK_SCHEMA}/g" 0_NeuralNetwork_CreateSchema.sql
sed -i "s/{NEURAL_NETWORK_USER}/${NEURAL_NETWORK_USER}/g" 0_NeuralNetwork_CreateSchema.sql
sed -i "s/{NEURAL_NETWORK_PWD}/${NEURAL_NETWORK_PWD}/g" 0_NeuralNetwork_CreateSchema.sql
sed -i "s/{NEURAL_NETWORK_SCHEMA}/${NEURAL_NETWORK_SCHEMA}/g" 1_NeuralNetwork_DDL.sql
export PGPASSWORD=${DB_ADMIN_PWD}
psql -h ${DB_SERVER} -p ${DB_PORT} -U ${DB_ADMIN_USER} -d postgres -f 0_NeuralNetwork_CreateSchema.sql
export PGPASSWORD=${NEURAL_NETWORK_PWD}
psql -h ${DB_SERVER} -p ${DB_PORT} -U ${NEURAL_NETWORK_USER} -d postgres -f 1_NeuralNetwork_DDL.sql
rm *.sql
